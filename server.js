//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var reqj = require('request-json');

var urlMlab = "https://api.mlab.com/api/1/databases/jvalladares";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlClientes = "collections/Clientes";
var urlUsuarios = "collections/Usuarios";

app.listen(port);

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "*");
	res.header("Access-Control-Allow-Methods", "*");
	res.header("Access-Control-Max-Age", "86400");
	next();
});

var movimientosJSON = require('./movimientosv2.json');

var clienteMlab = reqj.createClient(`${urlMlab}/${urlClientes}?${apiKey}`);
var usuarioMlab = reqj.createClient(`${urlMlab}/${urlUsuarios}?${apiKey}`);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post("/", function(req, res) {
  res.send("HTTP[POST]");
});

app.put("/", function(req, res) {
  res.send("HTTP[PUT]");
});

app.delete("/", function(req, res) {
  res.send("HTTP[DELETE]");
});

app.get("/clientes/:idcliente", function(req, res) {
  res.send("HTTP[GET]::ID_CLIENTE=" + req.params.idcliente);
});

app.get("/v1/movimientos", function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get("/v1/movimientos", function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get("/v2/movimientos", function(req, res) {
  res.json(movimientosJSON);
});

app.get("/v2/movimientos/:id", function(req, res) {
  console.log(req.params.id);
  res.json(movimientosJSON[req.params.id - 1]);
});

app.get("/v2/movimientosQuery", function(req, res) {
  console.log(req.query);
  res.send("recibido");
});

app.post("/v2/movimientos", function(req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send(nuevo);
});


app.get("/Clientes", function(req, res) {  
  clienteMlab.get('', function(err, resM, body) {
	if( err ) {
		console.log(body);
	} else {
		res.send(body);
	}
  });
});

app.post("/Clientes", function(req, res) {
	clienteMlab.post('', req.body, function(err, resM, body) {
		res.send(body);
	});
});

app.post("/Login", function(req, res) {
	var email = req.body.email;
	var password = req.body.password;

	var query = encodeURIComponent(`{"email":"${email}","password":"${password}"}`);
	var urlUsuario = `${urlMlab}/${urlUsuarios}?${apiKey}&q=${query}`;

	reqj.createClient(urlUsuario).get('', function(err, resM, body) {
		var success = !err && (body.length == 1);
		res.status(success? 200 : 403).send(success? "OK" : "NOK");
	});
});
