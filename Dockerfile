# Imagne base
FROM node:latest

# Directorio de el app en el contenedor
WORKDIR /app

# Copiado de archivos
ADD . /app

# Dependencias
RUN npm install

# Puerto que se expone
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm", "start"]
